# Provider
provider "aws" {
  region     = var.region
  access_key = var.access_key
  secret_key = var.secret_key
}

# Frontend bootstrap file
data "template_file" "user_data" {
  template = file("userdata.sh")

  vars = {
    backend_ip = aws_instance.devops-22-back.private_ip
  }
}

# Create EC2 frontend instance
resource "aws_instance" "devops-22-front" {
  ami                         = var.ami_front
  associate_public_ip_address = "true"
  disable_api_termination     = "false"
  instance_type               = "t2.micro"
  key_name                    = var.key_name
  monitoring                  = "false"
  subnet_id                   = var.subnet_id
  tags = {
    Name = "devops-22-front"
  }
  vpc_security_group_ids = [aws_security_group.devops-front-sg.id]
  user_data              = data.template_file.user_data.rendered
}

# Create EC2 backend instance
resource "aws_instance" "devops-22-back" {
  ami                         = var.ami_back
  associate_public_ip_address = "true"
  disable_api_termination     = "false"
  instance_type               = "t2.micro"
  key_name                    = var.key_name
  monitoring                  = "false"
  subnet_id                   = var.subnet_id
  tags = {
    Name = "devops-22-back"
  }
  vpc_security_group_ids = [aws_security_group.devops-back-sg.id]
}

# Create EC2 MySQL instance
resource "aws_instance" "devops-22-db" {
  ami                         = var.ami_db
  associate_public_ip_address = "true"
  disable_api_termination     = "false"
  instance_type               = "t2.micro"
  key_name                    = var.key_name
  monitoring                  = "false"
  subnet_id                   = var.subnet_id
  tags = {
    Name = "devops-22-db"
  }
  vpc_security_group_ids = [aws_security_group.devops-db-sg.id]
}

# Create security group for frontend server
resource "aws_security_group" "devops-front-sg" {
  vpc_id      = var.vpc_id
  name        = "devops-front-sg"
  description = "Security group for frontend server"
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = var.subnet_cidr_blocks
  }
  tags = {
    Name = "devops-front-sg"
  }
}

# Create security group for backend server
resource "aws_security_group" "devops-back-sg" {
  vpc_id      = var.vpc_id
  name        = "devops-back-sg"
  description = "Security group for backend server"
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = var.subnet_cidr_blocks
  }
  tags = {
    Name = "devops-back-sg"
  }
}

# Create security group for MySQL
resource "aws_security_group" "devops-db-sg" {
  vpc_id      = var.vpc_id
  name        = "devops-db-sg"
  description = "Security group for MySQL server"
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = var.subnet_cidr_blocks
  }
  tags = {
    Name = "devops-db-sg"
  }
}


output "instance_ips" {
  value = [
    aws_instance.devops-22-front.public_dns,
    aws_instance.devops-22-back.public_dns,
    aws_instance.devops-22-db.public_dns
  ]
}
