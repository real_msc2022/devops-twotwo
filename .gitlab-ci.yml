---
image: node:latest

cache:
  paths:
    - frontend/front/node_modules/
    - backend/back/node_modules/
    - backend/back/public/
    - backend/back/vendor/

before_script:
  - apt-get update -qy
  - apt-get install -y python-dev python-pip
  - git submodule update --init
  - pip install --upgrade ansible ansible-lint
  - ansible --version
  - ansible-lint --version
  - echo "please set runner job timeout..."

stages:
  - build
  - test
  - deploy

FrontBuild:
  stage: build
  image: trion/ng-cli
  before_script:
    - cd frontend/front
    - npm install
  script:
    - ng build --prod
  artifacts:
    paths:
      - frontend/front/node_modules/
      - frontend/front/dist/

FrontKarmaUnitTest:
  stage: test
  image: trion/ng-cli-karma
  allow_failure: true
  before_script:
    - cd frontend/front
  script:
    - ng test --watch=false
  artifacts:
    paths:
      - frontend/front/dist/

BackBuild:
  stage: build
  image: php:7.2-fpm
  before_script:
    - cd backend/back
    - apt-get update
    - chmod +x ./generate_env.sh
    # Install NodeJS
    - curl -sL https://deb.nodesource.com/setup_14.x | bash -
    - apt-get install -y nodejs git zip unzip
    # Install Compser
    - php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    - php composer-setup.php
    - php -r "unlink('composer-setup.php');"
  script:
    # Pull dependencies and optimize
    - php composer.phar install --optimize-autoloader --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
    - php composer.phar update
    - php artisan config:cache
    # Compile assets
    - npm install
    - npm run production
    # Generate .env
    - ./generate_env.sh
    - php artisan key:generate
  artifacts:
    paths:
      - backend/back/.env
      - backend/back/public/css/
      - backend/back/public/js/
      - backend/back/public/modules/
      - backend/back/public/mix-manifest.json
      - backend/back/vendor/

BackUnitTest:
  stage: test
  image: php:7.2-fpm
  allow_failure: true
  before_script:
    - cd backend/back
  script:
    - ./vendor/bin/phpunit
  artifacts:
    paths:
      - backend/back/.env

FrontDeploy:
  stage: deploy
  image: williamyeh/ansible:ubuntu18.04
  cache: {}
  before_script:
    # Setup SSH
    - 'which ssh-agent || ( apk add openssh-client )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan $FRONTEND_HOST >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - ansible-playbook -u $SSH_USER -i $FRONTEND_HOST, frontend-deploy-pb.yml
  when: manual

FrontRevert:
  stage: deploy
  image: williamyeh/ansible:ubuntu18.04
  cache: {}
  before_script:
    # Setup SSH
    - 'which ssh-agent || ( apk add openssh-client )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan $FRONTEND_HOST >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - ansible-playbook -u $SSH_USER -i $FRONTEND_HOST, frontend-revert-pb.yml
  when: manual

BackDeploy:
  stage: deploy
  image: williamyeh/ansible:ubuntu18.04
  cache: {}
  dependencies: []
  before_script:
    # Setup SSH
    - 'which ssh-agent || ( apk add openssh-client )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan $BACKEND_HOST >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - ansible-playbook -u $SSH_USER -i $BACKEND_HOST, backend-deploy-pb.yml
  when: manual

BackRevert:
  stage: deploy
  image: williamyeh/ansible:ubuntu18.04
  cache: {}
  before_script:
    # Setup SSH
    - 'which ssh-agent || ( apk add openssh-client )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan $BACKEND_HOST >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - ansible-playbook -u $SSH_USER -i $BACKEND_HOST, backend-revert-pb.yml
  when: manual

DatabaseMigrate:
  stage: deploy
  image: williamyeh/ansible:ubuntu18.04
  cache: {}
  before_script:
    # Setup SSH
    - 'which ssh-agent || ( apk add openssh-client )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan $BACKEND_HOST >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - ansible-playbook -u $SSH_USER -i $BACKEND_HOST, database-migrate-pb.yml
  when: manual