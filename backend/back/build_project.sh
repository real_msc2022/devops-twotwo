#!/bin/sh
composer install --optimize-autoloader --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
composer update
php artisan config:cache
npm install
npm run production
./generate_env.sh
# php artisan key:generate