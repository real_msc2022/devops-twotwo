#!/bin/bash

# Simple script to generate .env file during pipeline

# Application parameters
echo APP_NAME=Laravel >> .env
echo APP_ENV=production >> .env
echo APP_KEY= >> .env
echo APP_DEBUG=false >> .env
echo APP_URL=$APP_URL >> .env

# Logging parameters
echo LOG_CHANNEL=stderr >> .env

# Database parameters
echo DB_CONNECTION=$DB_CONNECTION >> .env
echo DB_HOST=$DB_HOST >> .env
echo DB_PORT=$DB_PORT >> .env
echo DB_DATABASE=$DB_DATABASE >> .env
echo DB_USERNAME=$DB_USERNAME >> .env
echo DB_PASSWORD=$DB_PASSWORD >> .env

# Miscellaneous
echo BROADCAST_DRIVER=log >> .env
echo CACHE_DRIVER=file >> .env
echo QUEUE_CONNECTION=sync >> .env
echo SESSION_DRIVER=file >> .env
echo SESSION_LIFETIME=120 >> .env