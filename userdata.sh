#!/bin/bash
export BACKEND_HOST=${backend_ip}
export DOLLAR='$'
sudo mv /etc/nginx/sites-available/default /etc/nginx/sites-available/default.copy
envsubst < /etc/nginx/sites-available/front.conf.template | sudo tee /etc/nginx/sites-available/default
sudo service nginx restart