### project based on ancible
#### How to start?
- ancible
- vm network

#### Features
- frontend
- backend
- MySql
- CICD
#### Manual testing 
- linting
  - possible before commit
  - is working through gitlab ci job ran on specific runner

#### Secrets
- secrets must be base64 hashed prior anything !!!
- ip adresses should be obfuscated as well as ports.
- any commited secrets would render the project **insecure**
- to enter a password in a shell:
  1. export nuclearcodes = trump
  2. echo $nuclearcodes
  3. sudo siri send missile to vladimir $nuclearcodes
  - that way the pw is not stored through *stdin*, but only in the session and bash history.

### Known issues
1. Issue
```m
Login to remote machine
Run sudo apt update
Look for a message like Err:15 http://dl.yarnpkg.com/debian stable Release.gpg The following signatures couldn't be verified because the public key is not available: NO_PUBKEY 4F77679369475BAA
Add key sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F77679369475BAA
Profit
```
2. Issue
```
Workaround

- name: certbot | Apt | Ubuntu | Install the Ubuntu PPA certbot/certbot
  apt_repository: repo=ppa:certbot/certbot codename={{ ansible_distribution_release }}
  when: ansible_distribution == 'Ubuntu'
instead of

- name: certbot | Apt | Ubuntu | Install the Ubuntu PPA certbot/certbot
  apt_repository: repo=ppa:certbot/certbot
  when: ansible_distribution == 'Ubuntu'
```